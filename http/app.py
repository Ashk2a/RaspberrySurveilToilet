from flask import Flask, render_template 
import memcache

#############################################################
#				Web server in Python with Flask				#
#############################################################

mc = memcache.Client(['127.0.0.1:11211'], debug=0) #init memcache to capture data send by other Python script
app = Flask(__name__) #init flask

def updateH(): #image depend of status (use or not)
	state = mc.get("wc_h")
	if state:
		return "man_red.svg"
	else:
		return "man_green.svg"

def updateF(): #image depend of status (use or not)
	state = mc.get("wc_f")
	if state:
		return "woman_red.svg"
	else:
		return "woman_green.svg"

def updateM(): #image depend of status (use or not)
	state = True;
	if state:
		return "woman_red.svg"
	else:
		return "woman_green.svg"

@app.route('/status') #Root to keep json with AJAX script
def status():
	return render_template('status.html', hstate = updateH(), fstate = updateF(), mstate = updateM())
		
@app.route('/') #Default root, the index
def index():	
    return render_template('index.html', hstate = updateH(), fstate = updateF(), mstate = updateM())

#MAIN : Running server on localhost adress
if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
app = Flask(__name__)
