﻿/*###########################################################
#		Display an image during X time and the toilet		#
#		  during X time with rotation between these			#
###########################################################*/

var t;

/* CONFIGURATION START */
var wc_duration = 180000; // SHOW 3 minute TOILET STATUS

var img_duration = 15000; // SHOW 15 seconds IMAGE CHOOSE
var img = "http://10.0.0.168:5000/static/trash.png"; // IMAGE URL

/* CONFIGURATION END */

/* Loop infinite */
function loader() {
	t = wc_duration;
	
	loadImg(img, img_duration);

	setTimeout(function() {
		loadWc();
		loader();
	}	
	, t);
	
}

/* Load toilet status and clear image*/
function loadWc() {
	$("body").css('background', 'none');
	$("#toilette").css('display', '');
}

/* Load image and clear toilet status */
function loadImg(background, duration) {
	setTimeout(function() {
		$("body").css('background', 'url("'+background+'") no-repeat');
		$("body").css('background-size', 'cover');
		$("#toilette").css('display', 'none');
	}	
	, t);
	
	t += duration;
}