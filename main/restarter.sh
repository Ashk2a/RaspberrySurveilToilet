#!/bin/bash
echo "#=============================#" 
echo "# Chargement...#"
echo "#=============================#"
echo
sleep 1 
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
while [ 1 == 1 ]
do
    echo "Demarrage checkWC"
    sudo python3 $DIR/checkWC.py

    echo
    echo "CheckWC Crash"
    killall checkWC.py
    sleep 1
    date=$(date "+%Y-%m-%d %H:%M:%S")
        echo Crash checkWC $date >> /tmp/crash.log
    echo "Redemarage du checkWC"
    echo
done
