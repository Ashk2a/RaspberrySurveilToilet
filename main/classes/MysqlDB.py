import pymysql.cursors

class MysqlDB:
	host = "localhost"
	db = "database"
	port = 3306
	user = "root"
	password = "root"
	
	connection = False
	def __init__(self):
		self.connection = pymysql.connect(host = self.host,
									user = self.user,
									password = self.password,
									db = self.db,
									charset = 'utf8mb4',
									cursorclass = pymysql.cursors.DictCursor)

	
	def insert(self, type, start, end, duration):
		sql = "INSERT INTO `wc_template` (`wc_type`, `date_start`, `date_end`, `duration`) VALUES (%s, %s, %s, %s)"
		self.connection.cursor().execute(sql, (type, start, end, duration))
		self.connection.commit()