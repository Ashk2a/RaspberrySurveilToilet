from TSL2561 import *
from MysqlDB import *
import memcache
import time
import datetime

class Toilet:
	name = ""
	old_busy = False
	busy = False
	
	
	t_start = 0
	t_end = 0
	
	d_start = 0
	d_end = 0
	
	mysqldb = False
	tsl = False
	memcache = memcache.Client(['127.0.0.1:11211'], debug=0)
	
	def __init__(self, name, addr):
		self.name = name
		self.mysqldb = MysqlDB()
		self.tsl = TSL2561(addr)
	
	def setCache(self):
		self.memcache.set(self.name, self.busy)
	
	def valideTime(self):
		if(self.t_stop - self.t_start >= 900):
			return False
		if(self.t_stop - self.t_start >= 10):
			return True
		
		return False
		
	def isBusy(self):
		if(self.tsl.calculateLux() > 200):
			self.old_busy = self.busy
			self.busy = True
		else:
			self.old_busy = self.busy
			self.busy = False
			
		return self.busy
	
		
	def changeStatus(self):
		time.sleep(0.1)
		if(self.isBusy()):
			if(self.busy != self.old_busy):
				self.t_start = time.time()
				self.d_start = datetime.datetime.now()
		else:
			if(self.busy != self.old_busy):
				self.t_stop = time.time()
				self.d_stop = datetime.datetime.now()
				if(self.valideTime()):
					self.mysqldb.insert(self.name, self.d_start, self.d_stop, self.t_stop - self.t_start)
		
		self.setCache()
			